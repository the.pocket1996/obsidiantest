# User Groups
Area ID | Area Name | Sub Area Id | Sub Area Name | What this Effects/ When this is checked
-| -| - |- | -
1 | Tilling | 1 | Sale | Press Start sale
1 | Tilling | 2 | Supervisor | Press Supervisor Menu
1 | Tilling | 3 | Void | Press Void Sale</br>Press Void Item</br>Press Void Last 
2 | Budgets | 1 | Insert Budgets |  ![[Budgets#How to configure]] ^[ #V7_0_126 ] 
3 | Donors | 1 | View Donors | Donor Browse ![[Donor List#Donor Browse]] ^[ #V7_1_192 ]
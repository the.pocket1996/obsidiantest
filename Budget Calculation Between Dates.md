# Budget Calculation Between dates 
 #Dashboard #GiftAid
We store the budgets for a particular week. This Doesn't work well for between dates so we use the open Hours to split out the budgets into daily budgets and this way we can add these daily budgets to get the budgets between date.

## The calculation
Firstly we take the Total number of hours open in a week.
We then get the number of hours in the given range. 
We then Do the following calculation

$$Partial Week Budget = \left( \frac{Budget} {Hours Open in Week}\right) \times Hours Open Bewteen Dates $$


## Example
| Day | Hours open | 
| -| - |
| Monday | 8 |
| Tuesday | 8 |
| Wednesday | 8 |
| Thursday | 8 |
| Friday | 7 |
| Saturday | 4 |
| Sunday | 0 |

If the Between dates is thursday to monday. and the budget for the weeks is 400.

 Sum of Open Hours in week  `8 + 8 + 8 + 8 + 6 + 2 + 0 =`  `40`  
 Hours Open In range `8 + 6 + 2 + 0 + 8 =` `24`

So the Budget for that Range is `(400 / 40) * 24` = `240`


# Dashboard
 #Dashboard 
The dashboard lets charities see there figure live.

## About 
![[Pasted image 20220315190341.png]]

Above is a picture of the dashboard

## Where does the information come from

The information for the dashboard is built up in the End of day and is put into **DASHBFIGS**


| Info | Description |Calculation |
|-----|-----------|--|
| 1 |  Total Gift aid sales This Week | Sum of SalesValue from CDSALES | 
| 2 | Number of sign ups This week| Count of Donors where Inserted Date Between This week Calculation|



![[Wedno#Getting the current week no]]

## Budgets 
The dashboard will get Budget information from the [[Budgets]] feature

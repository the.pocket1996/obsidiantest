# Budgets
 #V7_0_126 #Dashboard 
Budgets allow charities to track their sale progress by comparing it against predefined targets.

## How to configure 

^a8970b

The Back Office Screen > Setup > Dashboard Budgets
![[Pasted image 20220315203334.png]]

## budget calculation on the dashboard
The budgets on the dashboard will be split by day base on the Open Hour

## Calculating Between Dates Budgets 
![[Budget Calculation Between Dates]]

## Things to test
[ x ] The budget Calculation gives the same result as example
[ - ] The budgets can be imported and exported correctly
[   ]  Only Users With the budget User Group can import and export budgets


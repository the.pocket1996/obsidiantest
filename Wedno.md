# WEDNO (Week Ending Dates No)
WEDNO holds a the dates of the week ending dates based on the customers week End Day and year End Date

## settings
| File | Setting ID | Setting | Next Number| 
|-|-|-|
| Mssystem | YearEndDate | DD/MM is the format you need to put you do not need to put a year as this will be every year | N/a|
| Mssystem | WeekEndDay | N/a| 0 - 6, 0 = Monday, 1 = Tuesday, ... 6 = Sunday|


## Getting the current week no
To get the current  week no you can use the below logic

``` C# 
public int GetWeekNo(int date) {
	// Logic for Getting Current Week where Passed date is today
}

```

Explanation This is something ==Very Important== So I have Highlighted it  
